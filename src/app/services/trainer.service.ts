import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { setStorage, getStorage } from '../utils/localStorage';

@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  /**
   * Trainer adds a new pokemon object to his collection of pokemons and updates the collectedPokemon variable from localstorage
   * @param pokemon 
   */
  public addToCollection(pokemon: Pokemon): void {
    let collection = this.getCollection();
    if (!this.isInCollection(pokemon.name)) {
      collection.push(pokemon);
    }
    setStorage('collectedPokemon', collection);
  }

  public getCollection(): Pokemon[] {
    return getStorage('collectedPokemon');
  }

  public isInCollection(pokemonName: string): boolean {
    return this.getCollection().some((poke) => poke.name === pokemonName);
  }

    /**
   * Trainer removes a pokemon object from his collection of pokemons and updates the collectedPokemon variable from localstorage
   * @param pokemon 
   */
  public removeFromCollection(pokemon: Pokemon): void {
    let collection = this.getCollection();
    collection = collection.filter((poke) => {
      poke.name !== pokemon.name;
    });
    setStorage('collectedPokemon', collection);
  }
}
