import { Injectable } from '@angular/core';
import { getStorage, setStorage, clearStorage } from '../utils/localStorage';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  /**
   * 
   * When the user logs in the name of the trainer is stored in local storage 
   * and also the collectedPokemon variables is initialized in local storage as an empty array
   */
  public login(trainerName: string): void {
    setStorage('trainerName', trainerName);
    setStorage('collectedPokemon', []);
  }

  /**
   * Verifies if the trainer's name exists in localStorage (it means that is logged in)
   */
  public active(): boolean {
    const trainer = getStorage('trainerName');

    return Boolean(trainer);
  }

  public getTrainerName(): string {
    return getStorage('trainerName');
  }

  public logout(): void {
    clearStorage();
  }
}
