/**
 * Sets on local storage an object with key and value, where the value is stringified
 * @param key 
 * @param value 
 */
export const setStorage = (key: string, value: any) => {
  const json = JSON.stringify(value);
  localStorage.setItem(key, json);
};

/**
 * Returns the object from localstorage found by the @param key received as input
 * @param key 
 * @returns 
 */
export const getStorage = (key: string) => {
  const storedValue = localStorage.getItem(key);
  if (!storedValue) return false;

  return JSON.parse(storedValue);
};

export const clearStorage = () => {
  localStorage.clear();
};
